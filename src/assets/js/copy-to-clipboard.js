;(function (factory) {
    'use strict'
    new factory(window.jQuery, window.fafcms)
})(function ($, fafcms) {
    'use strict'

    fafcms.prototype.events['init'].push(function () {
        $('body').on('click', '.copy-text', function(e) {
            e.preventDefault()
            var $copyButton = $(this)
            var $target
            var remove = false
            var message = $('html').attr('lang') === 'de'?'Text wurde in Zwischenablage kopiert!':'Text has been copied!'

            if (typeof $copyButton.data('copy-text') !== 'undefined') {
                $target = $('<input>')
                $('body').append($target)
                $target.val($copyButton.data('copy-text').trim()).select()
                remove = true
            } else {
                $target = $($copyButton.data('copy-target'))
            }

            $target[0].select()
            document.execCommand('copy')

            if (remove) {
                $target.remove()
            }

            if (typeof Materialize !== 'undefined') {
                Materialize.toast(message, 4000, 'green')
            }
            else if (typeof M !== 'undefined') {
                M.toast({html: message, classes: 'green'})
            }
        })
    })

    return fafcms
})
