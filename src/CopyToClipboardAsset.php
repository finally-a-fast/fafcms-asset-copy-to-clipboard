<?php
namespace fafcms\assets\copytoclipboard;

use yii\web\AssetBundle;

class CopyToClipboardAsset extends AssetBundle
{
    public $sourcePath = __DIR__.'/assets';

    public $js = [
        'js/copy-to-clipboard.js',
    ];

    public $depends = [
        'fafcms\assets\init\InitAsset',
    ];
}
